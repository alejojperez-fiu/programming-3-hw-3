# The compiler: gcc for C program
CC = gcc

# the build target executable:
TARGET = DealCardHands.out

build:
	$(CC) MainCardHands.c CardsDeckOperations.c -w -o $(TARGET)

clean:
	rm $(TARGET)