/*=============================================================================
|   Source code:  CardsDeckOperations.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #3 Assignment 3
|*===========================================================================*/

/*=============================================================================
| INCLUDES +===================================================================
|*===========================================================================*/
#include "CommonFunctionalities.h"

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

/**
 * Helper function that calculates the amount of card required to play based on
 * the amont of players and the amount of cards per player. The formula is:
 * 
 * numberOfPlayers * numberOfCards
 * 
 * @return The amount of cards required based of players and cards per player
 */
int AmountOfCardRequiredForPlayers(int numberOfPlayers, int numberOfCards)
{
    return numberOfPlayers * numberOfCards;
}

/**
 * Helper function to fill out the players deck based on the amount of players,
 * the amount of cards per player, and a valid deck of cards.
 * 
 * @param deck The deck from where the cards are going to be dealt
 * @param playersDeck The deck where the cards of each player are going to be
 *                    stored
 * @param players The amount of players
 * @param cardsPerPlayer The amount of cards per player
 */
void DealCardsToPlayers(
    const char deck[][CARD_ARRAY_SIZE], 
    char playersDeck[][MAX_INPUT_VALUE][CARD_ARRAY_SIZE], 
    int players, 
    int cardsPerPlayer
)
{
    int playersCounter = 0, deckCurrentPosition = 0;

    for(playersCounter = 0; playersCounter < players; playersCounter++)
    {
        int cardsCounter = 0;

        for(cardsCounter = 0; cardsCounter < cardsPerPlayer; cardsCounter++)
        {
            playersDeck[playersCounter][cardsCounter][CARD_RANK_POSITION] =
            deck[deckCurrentPosition][CARD_RANK_POSITION];

            playersDeck[playersCounter][cardsCounter][CARD_SUIT_POSITION] =
            deck[deckCurrentPosition][CARD_SUIT_POSITION];

            deckCurrentPosition++;
        }
    }
}

/**
 * Helper function to find the amount of cards in a deck. It multiplies the
 * amount of cards per suit times the amount of suits.
 * 
 * @retrun The amount of cards in a deck
 */
int GetAmountOfCards()
{
    static const int cardsInDeck = CARD_SUITS * CARD_RANKS;

    return cardsInDeck;
}

/**
 * Get a random index position in a deck of cards
 */
int GetRandomPositionInDeck()
{
    static bool seeded = false;

    if(seeded == false)
    {
        srand(time(NULL));
        seeded = true;
    }

    return rand() % GetAmountOfCards();
}

/**
 * Helper function to obtain the rank of a card based on the initial position
 * in the deck array
 * 
 * @param positionInDeck The position the we want the rank of
 * 
 * @return The rank character based on the position
 */
char InitializeCardRank(int positionInDeck)
{
    static const char ranks[] = {
        'A', '2', '3', '4', '5', '6',
        '7', '8', '9', 'T', 'J', 'Q', 'K'
    };

    return ranks[positionInDeck % CARD_RANKS];
}

/**
 * Helper function to obtain the suit of a card based on the initial position
 * in the deck array
 * 
 * @param positionInDeck The position the we want the suit of
 * 
 * @return The suit character based on the position
 */
char InitializeCardSuit(int positionInDeck)
{
    static const char suits[] = {
        'S', 'H', 'C', 'D'
    }; 

    return suits[positionInDeck / CARD_RANKS];
}

/**
 * Helper function to fill the deck content with cards in an organized manner
 * 
 * @param deck The deck that is going to be filled
 */
void InitializeDeck(char deck[][CARD_ARRAY_SIZE])
{
    int cardPosotionInDeck = 0;

    for(
        cardPosotionInDeck = 0;
        cardPosotionInDeck < GetAmountOfCards();
        cardPosotionInDeck++
    )
    {        
        deck[cardPosotionInDeck][CARD_SUIT_POSITION] =
        InitializeCardSuit(cardPosotionInDeck);

        deck[cardPosotionInDeck][CARD_RANK_POSITION] =
        InitializeCardRank(cardPosotionInDeck);
    }
}

/**
 * Helper function to print the suit and rank of a card
 * 
 * @param card The card that is going to be printed
 */
void PrintCard(const char card[])
{
    printf("[ %c-%c ]", card[CARD_RANK_POSITION], card[CARD_SUIT_POSITION]);
}

/**
 * Helper function to print the contents of a deck of cards
 * 
 * @param deck The deck of card tht is going to be printed
 */
void PrintDeck(const char deck[][CARD_ARRAY_SIZE])
{
    int cardPosotionInDeck = 0;

    for(
        cardPosotionInDeck = 0;
        cardPosotionInDeck < GetAmountOfCards();
        cardPosotionInDeck++
    )
    {
        if(cardPosotionInDeck % CARD_RANKS != 0)
        {
            printf(" ");
        }

        PrintCard(deck[cardPosotionInDeck]);

        if(cardPosotionInDeck % CARD_RANKS == CARD_RANKS - 1)
        {
            printf("\n");
        }
    }
}

/**
 * Helper function that prints the input received to the user, so (s)he knows
 * waht are the parameters being used when computing the result
 */
void PrintInput(int cardsPerPlayer, int players)
{
    PrintTextSuccess("Input:\n");
    printf("Cards per player: %d\n", cardsPerPlayer);
    printf("Number of players: %d\n\n", players);
}

/**
 * Helper funciton to print the content of the players' deck of cards
 */
void PrintPlayersDeck(
    const char playersDeck[][MAX_INPUT_VALUE][CARD_ARRAY_SIZE], 
    int players, 
    int cardsPerPlayer
)
{
    int playersCounter = 0;

    for(playersCounter = 0; playersCounter < players; playersCounter++)
    {
        int cardsCounter = 0;
        int playerNumber = playersCounter + 1;

        printf("Player %d] - ", playerNumber);

        for(cardsCounter = 0; cardsCounter < cardsPerPlayer; cardsCounter++)
        {
            if(cardsCounter != 0)
            {
                printf(" ");
            }

            PrintCard(playersDeck[playersCounter][cardsCounter]);
        }

        puts("");
    }
}

/**
 * This function accepts the amount of cards per player and the amount of
 * players, and it first prints to the user what is the input that it will work
 * with, the amout of acrds in the deck. Then it creates a new deck of cards and
 * prints its content to the end user. Later, that deck is shuffled and printd 
 * again, so the user sees the final state of the deck. Finally, it creates a
 * hand for each user and deals the cards from the shuffled deck. Once it
 * finishes, then it prints the cards of each player.
 * 
 * @param cardsPerPlayer The amount of cards per player
 * @param players The amount of players
 */
void ProcessInput(int cardsPerPlayer, int players)
{
    PrintInput(cardsPerPlayer, players);

    int amountOfCardsInDeck = GetAmountOfCards();
    char deck[amountOfCardsInDeck][CARD_ARRAY_SIZE];

    PrintTextSuccess("Deck Size: ");
    printf("%d\n\n", amountOfCardsInDeck);

    InitializeDeck(deck);
    PrintTextSuccess("Original Ordered Deck:\n");
    PrintDeck(deck);

    puts("");

    ShuffleDeck(deck);
    PrintTextSuccess("Random Shuffled Deck:\n");
    PrintDeck(deck);

    puts("");

    char playersDecks[players][cardsPerPlayer][CARD_ARRAY_SIZE];
    
    DealCardsToPlayers(deck, playersDecks, players, cardsPerPlayer);
    PrintTextSuccess("Player Hands: (dealt from top/front of deck):\n");
    PrintPlayersDeck(playersDecks, players, cardsPerPlayer);
}

/**
 * Helper function that accepts a deck of cards and shuffles it.
 * 
 * Suffle Algorithm: Following the instructions discussed by William Feild, we
 * are going to iterate through the array of cards, and in each iteration we are
 * going to find a random position in the array where the value of the current
 * iteration is going to be swapped with the randomly obtained the postion. By
 * doing this we are giving the oportunity to each card to be moved from its
 * original position, but also we have the posibility that the same card could
 * be returned to its original position as well.
 * 
 * @param deck The deck of cards that is going to be shuffled
 */
void ShuffleDeck(char deck[][CARD_ARRAY_SIZE])
{
    int deckPosition = 0;

    for(deckPosition = 0; deckPosition < GetAmountOfCards(); deckPosition++)
    {
        SwapCardsPositionInDeck(deck, deckPosition, GetRandomPositionInDeck());
    }
}

/**
 * Get two positions in a deck of cards and swap the values. Create a temporal
 * value holder that is going to hold the value of the original postion, then
 * fill the original postion with the swap postion, and finally put the hold
 * value in the swap postion.
 * 
 * @param deck The deck where the cards are
 * @param deckPosition The original postion that is going to be swapped
 * @param swapPosition The position that is going to be swapped with the
 *                     original postion
 */
void SwapCardsPositionInDeck(
    char deck[][CARD_ARRAY_SIZE],
    int deckPosition,
    int swapPosition
)
{
    char temporalCard[CARD_ARRAY_SIZE] = {'\0'};

    temporalCard[CARD_RANK_POSITION] = 
    deck[deckPosition][CARD_RANK_POSITION];
    temporalCard[CARD_SUIT_POSITION] = 
    deck[deckPosition][CARD_SUIT_POSITION];

    deck[deckPosition][CARD_RANK_POSITION] = 
    deck[swapPosition][CARD_RANK_POSITION];
    deck[deckPosition][CARD_SUIT_POSITION] =
    deck[swapPosition][CARD_SUIT_POSITION];

    deck[swapPosition][CARD_RANK_POSITION] =
    temporalCard[CARD_RANK_POSITION];
    deck[swapPosition][CARD_SUIT_POSITION] =
    temporalCard[CARD_SUIT_POSITION];
}

/**
 * Helper function that validates the amount of cards allowed by the program
 * 
 * @return Wheter the amount of cards is valid or not
 */
bool ValidateAmountOfCards(int cards)
{
    if(cards < MIN_INPUT_VALUE || cards > MAX_INPUT_VALUE)
    {
        PrintTextDanger(
            "The amount of cards per player specified is not valid.\n"
            "Refer to the program description to see the allowed values.\n\n"
        );

        return false;
    }
}

/**
 * Helper function that validates the amount of players allowed by the program
 * 
 * @return Wheter the amount of players is valid or not
 */
bool ValidateAmountOfPlayers(int players)
{
    if(players < MIN_INPUT_VALUE || players > MAX_INPUT_VALUE)
    {
        PrintTextDanger(
            "The amount of cards per player specified is not valid.\n"
            "Refer to the program description to see the allowed values.\n\n"
        );

        return false;
    }
}

/**
 * Helper function that validates that there is enough cards to deal to each
 * player
 * 
 * @return Wheter the amount of cards is enough to play
 */
bool ValidateEnoughCardsToPlay(int cards, int players)
{
    if(AmountOfCardRequiredForPlayers(cards, players) > GetAmountOfCards())
    {
        PrintTextDanger(
            "There is not enough cards for the amount of players\n"
            "and the amount of cards per player specified.\n\n"
        );

        return false;
    }
}