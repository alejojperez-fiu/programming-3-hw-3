/*=============================================================================
|   Source code:  CommonFunctionalities.h
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #3 Assignment 3
|*===========================================================================*/

#ifndef CommonFunctionalities_H

/*=============================================================================
| INCLUDES ====================================================================
|*===========================================================================*/
/*
 * <stdio.h> It is being used in order to output information to the end user
 * <stdio.h> It is being used to convert string to integer, and to be able to
 *           generate random numbers
 * <time.h> It is used to get the current time
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*=============================================================================
| CONSTANTS ===================================================================
|*===========================================================================*/

/**
 * Helper define to avoid including this header file twice in the program
 */
#define CommonFunctionalities_H

#define CARD_SUITS 4 // The amount of unique suits that exists in a card deck 
#define CARD_RANKS 13 // The amount of card ranks
#define MIN_INPUT_VALUE 1 // The minimum value allowed for the amount of players
                          // and the amount of cards per player
#define MAX_INPUT_VALUE 13 // The maximum value allowed for the amount of 
                           //players and the amount of cards per player
#define CARD_SUIT_POSITION 0 // The position of the argument that contains the
                             // card suit in the deck array
#define CARD_RANK_POSITION 1 // The position of the argument that contains the
                             // card rank in the deck array
#define CARD_ARRAY_SIZE 2 // The size of the array that each card must have in
                          // order to hold the suit and the rank

#define CONSOLE_COLOR_GREEN "\x1b[32m" // The value used to print green font
                                      // color to the console
#define CONSOLE_COLOR_RESET "\x1b[0m" // The value used to reset the console's
                                      // font color
#define CONSOLE_COLOR_RED "\x1B[31m" // The value used to print red font color
                                     // to the console
#define CONSOLE_COLOR_YELLOW "\x1B[33m" // The value used to print yellow font 
                                        // color to the console

#define ERROR 0
#define INPUT_AMOUT_OF_CARDS_POSITION 1 // The position of the argument that
                                        // contains the amount of cards per
                                        // player in the console's arguments
                                        // array
#define INPUT_AMOUT_OF_PLAYERS_POSITION 2 // The position of the argument that
                                          // contains the amount of players in 
                                          // the console's arguments array
#define MIN_INPUT_VALID_ARGUMENTS_COUNT 3 // The minimum amount of arguments
                                          // required for the program to work
#define SUCCESS 0

/*=============================================================================
| CUSTOM TYPES ================================================================
|*===========================================================================*/

/**
 * Replicate the Boolean functionality from other programming languages
 */
typedef enum { 
    false, 
    true 
} bool;

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

int AmountOfCardRequiredForPlayers(int, int);
void DealCardsToPlayers(
    const char [][CARD_ARRAY_SIZE],
    char [][MAX_INPUT_VALUE][CARD_ARRAY_SIZE],
    int,
    int
);
int GetAmountOfCards();
int GetRandomPositionInDeck();
char InitializeCardRank(int);
char InitializeCardSuit(int);
void InitializeDeck(char [][CARD_ARRAY_SIZE]);
void PrintCard(const char[]);
void PrintDeck(const char[][CARD_ARRAY_SIZE]);
void PrintInput(int, int);
void PrintPlayersDeck(
    const char [][MAX_INPUT_VALUE][CARD_ARRAY_SIZE],
    int,
    int
);
void PrintProgramHeader();
void PrintTextDanger(char[]);
void PrintTextSuccess(char[]);
void PrintTextWarning(char[]);
void PrintTextWithColor(char[], char[]);
void ProcessInput(int, int);
void ShuffleDeck(char [][CARD_ARRAY_SIZE]);
void SwapCardsPositionInDeck(char [][CARD_ARRAY_SIZE], int, int);
bool ValidateAmountOfCards(int);
bool ValidateAmountOfPlayers(int);
bool ValidateConsoleArgumentsCount(int);
bool ValidateConsoleValues(int, char**, int*, int*);
bool ValidateEnoughCardsToPlay(int, int);

#endif